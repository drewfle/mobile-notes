## Default value

```js
const userSettings = {nightMode: true, fontSize: 'large'}

const {
  nightMode = false,
  language = 'en',
  fontSize = 'normal'
} = userSettings
```